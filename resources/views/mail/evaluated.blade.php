Hi,

<p>The following objective(s) have been evaluated:</p>

<p>Employee: {{ $objectiveEmployee }}</p>

<p>Objective Id: {{ $objectiveId }}</p>

<p><b>Objective(s):</b>
<br />{{ $objectiveText }}</p>

<hr>

<p>Objective(s) Evaluations: {{ $evaluation }}</p>

<p><b>Comment(s):</b>
<br />{{ $evaluationComment }}</p>

<p>Does this employee exhibit the MaritzCX Core Values?
<br />{{ $mcxCoreValues }}</p>

<p>Does this employee demonstrate a commitment to their Personal Development?
<br />{{ $personalCommitment }}</p>

<p>Evaluated by {{ $manager }}, {{ $objectiveDate }}</p>

Thanks!
